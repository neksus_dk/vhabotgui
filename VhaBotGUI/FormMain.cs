﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Xml;

namespace VhaBotGUI {
    public partial class FormMain : Form {
        private delegate void AddTextCallback(string text);
        private Process vhaBotProcess;
        private bool runFailed;
        private string firstBot = string.Empty;

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        public FormMain() {
            InitializeComponent();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load("config.xml");
            string str = xmlDocument.SelectSingleNode("//Configuration/Bot/Character").FirstChild.Value.ToLower();
            string str2 = xmlDocument.SelectSingleNode("//Configuration/Bot/Dimension").FirstChild.Value.ToLower();
            this.firstBot = str + "@" + str2;
        }

        private void vhaBotProcess_OutputDataReceived(object sender, DataReceivedEventArgs e) {
            this.AddText(e.Data);
        }
        private void AddText(string text) {
            if (this.richTextBoxOutPut.InvokeRequired) {
                FormMain.AddTextCallback method = new FormMain.AddTextCallback(this.AddText);
                base.Invoke(method, new object[]
				{
					text
				});
                return;
            }
            if (!string.IsNullOrEmpty(text)) {
                TextBox textBox = new TextBox();
                textBox.Text = text;
                if (textBox.Text.ToLower().Contains("[" + this.firstBot + "]")) {
                    int num = textBox.Text.ToLower().IndexOf(this.firstBot);
                    this.richTextBoxOutPut.SelectionColor = Color.Black;
                    this.richTextBoxOutPut.AppendText(textBox.Text.Substring(0, num));
                    this.richTextBoxOutPut.SelectionColor = Color.Red;
                    this.richTextBoxOutPut.AppendText(this.firstBot);
                    this.richTextBoxOutPut.SelectionColor = Color.Black;
                    this.richTextBoxOutPut.AppendText(textBox.Text.Substring(num + 12) + Environment.NewLine);
                }
                else {
                    if (textBox.Text.ToLower().Contains("[core]")) {
                        int num2 = textBox.Text.ToLower().IndexOf("core");
                        this.richTextBoxOutPut.SelectionColor = Color.Black;
                        this.richTextBoxOutPut.AppendText(textBox.Text.Substring(0, num2));
                        this.richTextBoxOutPut.SelectionColor = Color.Blue;
                        this.richTextBoxOutPut.AppendText("Core");
                        this.richTextBoxOutPut.SelectionColor = Color.Black;
                        this.richTextBoxOutPut.AppendText(textBox.Text.Substring(num2 + 4) + Environment.NewLine);
                    }
                    else {
                        this.richTextBoxOutPut.SelectionColor = Color.Black;
                        this.richTextBoxOutPut.AppendText(textBox.Text + Environment.NewLine);
                    }
                }
                this.ScrollToEnd(this.richTextBoxOutPut);
            }
        }

        protected override void WndProc(ref Message m) {
            if (m.Msg == 274 && m.WParam == (IntPtr)61472) {
                this.TrayMini();
                return;
            }
            base.WndProc(ref m);
        }

        private void showHideToolStripMenuItem_Click(object sender, EventArgs e) {
            this.TrayMini();
        }

        private void TrayMini() {
            if (base.WindowState.Equals(FormWindowState.Minimized)) {
                base.Show();
                base.WindowState = FormWindowState.Normal;
                this.ScrollToEnd(this.richTextBoxOutPut);
                return;
            }
            base.Hide();
            base.WindowState = FormWindowState.Minimized;
        }

        private void notifyIconMain_MouseClick(object sender, MouseEventArgs e) {
            MouseButtons button = e.Button;
            if (button != MouseButtons.Left) {
                return;
            }
            this.TrayMini();
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e) {
            if (!this.runFailed) {
                this.vhaBotProcess.Kill();
            }
        }

        private void terminateToolStripMenuItem_Click(object sender, EventArgs e) {
            base.Close();
        }

        private void ScrollToEnd(RichTextBox rtb) {
            IntPtr wParam = new IntPtr(7);
            IntPtr lParam = new IntPtr(0);
            FormMain.SendMessage(rtb.Handle, 277u, wParam, lParam);
        }

        private void startBot() {
            try {
                this.vhaBotProcess = new Process();
                this.vhaBotProcess.OutputDataReceived += new DataReceivedEventHandler(this.vhaBotProcess_OutputDataReceived);
                this.vhaBotProcess.StartInfo.CreateNoWindow = true;
                this.vhaBotProcess.StartInfo.FileName = "vhabot.exe";
                this.vhaBotProcess.StartInfo.RedirectStandardOutput = true;
                this.vhaBotProcess.StartInfo.UseShellExecute = false;
                this.vhaBotProcess.EnableRaisingEvents = true;
                this.vhaBotProcess.Start();
                this.vhaBotProcess.BeginOutputReadLine();
            }
            catch (Exception ex) {
                this.runFailed = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void FormMain_Load(object sender, EventArgs e) {
            this.startBot();
        }
    }
}
